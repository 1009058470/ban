import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TotalServiceTest {
    TotalService totalService;

    @BeforeEach
    void initAll(){
        totalService = new TotalService();
    }

    @Test
    void test_for_none_throws_return_0() {
        assertEquals(0,totalService.getTotalScore());
    }

    @Test
    void test_for_when_one_throw_should_return_score() {
        totalService.FrameScore(5);
        assertEquals(5,totalService.getTotalScore());
    }


    @Test
    void test_for_when_two_throw_should_return_add_two_score() {
        int arr[] = {1,2};
        totalService.FrameScore(arr);
        assertEquals(3,totalService.getTotalScore());
    }

    @Test
    void test_for_10_frames_without_strike_and_spare() {
        int secondThrow = 0;
        int firstThrow = 1;
        Frames frames = new Frames(firstThrow,secondThrow);
        List<Frames> list = new ArrayList<>();
        for(int i = 0;i<10;i++){
            list.add(frames);
        }
        totalService.lineScore(list);
        assertEquals(10,totalService.getTotalScore());
    }

    @Test
    void test_when_first_frame_is_spare_return_10_plus() {
        int firstThrow = 2;
        int secondThrow = 8;
        List<Frames> list = new ArrayList<>();
        Frames frame1 = new Frames(firstThrow,secondThrow);
        list.add(frame1);
        Frames framesOther = new Frames(1,0);
        for(int i = 0;i<9;i++){
            list.add(framesOther);
        }
        totalService.lineScore(list);
        assertEquals(20,totalService.getTotalScore());
    }

    @Test
    void test_when_10_frames_with_spare_without_strike_and_return() {  // tasking 5,6是一样的
        List<Frames> list = new ArrayList<>();
        Frames framesOther = new Frames(1,0);
        for(int i = 0;i<8;i++){
            list.add(framesOther);
        }
        list.add(new Frames(2,8));
        list.add(new Frames(1,0));
        totalService.lineScore(list);
        assertEquals(20,totalService.getTotalScore());
    }

    @Test
    void test_when_10_and_last_frame_is_spare_without_no_strick() {
        List<Frames> list = new ArrayList<>();
        Frames frames = new Frames(1,0);
        for(int i = 0; i< 9;i++){
            list.add(frames);
        }
        list.add(new Frames(2,8));
        list.add(new Frames(1,0));
        totalService.lineScore(list);
        assertEquals(20,totalService.getTotalScore());
    }


    @Test
    void test_only_first_frame_is_strike() {
        List<Frames> list = new ArrayList<>();
        list.add(new Frames(10,0));
        Frames frames = new Frames(1,1);
        for(int i = 0; i< 9;i++){
            list.add(frames);
        }
        totalService.lineScore(list);
        assertEquals(30,totalService.getTotalScore());
    }

    @Test
    void test_10_with_strike_and_last_is_not() {
        List<Frames> list = new ArrayList<>();

        Frames frames = new Frames(1,1);
        for(int i = 0; i< 8;i++){
            list.add(frames);
        }
        list.add(new Frames(10,0));
        list.add(new Frames(1,1));
        totalService.lineScore(list);
        assertEquals(30,totalService.getTotalScore());
    }


    @Test
    void test_10_and_last_is_strike() {
        List<Frames> list = new ArrayList<>();
        Frames frames = new Frames(1,1);
        for(int i = 0; i< 9;i++){
            list.add(frames);
        }
        list.add(new Frames(10,0));
        list.add(new Frames(1,0));
        list.add(new Frames(1,0));
        totalService.lineScore(list);
        assertEquals(30,totalService.getTotalScore());
    }

    @Test
    void test_10_with_10_strikes() {
        List<Frames> list = new ArrayList<>();
        Frames frames = new Frames(10,0);
        for(int i = 0; i< 10;i++){
            list.add(frames);
        }
        list.add(new Frames(10,0));
        list.add(new Frames(10,0));
        totalService.lineScore(list);
        assertEquals(300,totalService.getTotalScore());
    }

    @Test
    void other_test() {
        List<Frames> list = new ArrayList<>();
        Frames frames = new Frames(2,8);
        for(int i = 0;i<10;i++){
            list.add(frames);
        }
        list.add(new Frames(2,0));
        totalService.lineScore(list);
        assertEquals(120,totalService.getTotalScore());
    }
}