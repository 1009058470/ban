public class Frames {
    private int firstThrow = 0; //在第一次为10第二次没有投
    private int secondThrow = -1;

    public Frames(int firstThrow, int secondThrow) {
        this.firstThrow = firstThrow;
        this.secondThrow = secondThrow;
    }

    public int getFirstThrow() {
        return firstThrow;
    }

    public void setFirstThrow(int firstThrow) {
        this.firstThrow = firstThrow;
    }

    public int getSecondThrow() {
        return secondThrow;
    }

    public void setSecondThrow(int secondThrow) {
        this.secondThrow = secondThrow;
    }


    public int getTotalFrame() {
        return secondThrow >= 0 ? firstThrow+secondThrow : firstThrow;
    }
}
