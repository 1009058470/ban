import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class TotalService {
    private int totalScore = 0;

    public int getTotalScore() {
        return totalScore;
    }

    public void FrameScore(int firstThrow) {
        totalScore = firstThrow;
    }

    public void FrameScore(int[] throwsBall){
        Arrays.stream(throwsBall).forEach(value -> totalScore+=value);
    }


    public void lineScore(List<Frames> frames) {
        int extra = 0;
        for(int i = 0; i<frames.size() && i<10;i++){
            if(frames.get(i).getFirstThrow()==10){
                extra += frames.get(i+1).getTotalFrame();
                if(frames.get(i+1).getFirstThrow()==10){
                    extra += frames.get(i+2).getFirstThrow();
                }
                else if(i==9){
                    extra += frames.get(i+2).getFirstThrow();
                }
            }
            else if(frames.get(i).getTotalFrame()==10 && i <= 9){
                extra += frames.get(i+1).getFirstThrow();
            }
        }
        frames.stream().forEach(frame -> totalScore+=frame.getTotalFrame());
        totalScore += extra;
        int otherScore = 0;
        if(frames.size()>10){
            for(int i = 10;i<frames.size();i++){
                otherScore+=frames.get(i).getFirstThrow();
            }
        }
        totalScore -= otherScore;
    }
}
